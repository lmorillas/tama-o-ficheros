# coding: utf-8

from csv import reader, writer
import requests
import os

#ruta de los ficheros csv
RUTA = 'ficheros'
# campo que contiene la url
CAMPO = 3

def sizeof(url):
	'''
	Devuelve el tamaño de un fichero a partir de la cabecera http
	'''
	r = requests.head(url)
	return r.headers.get('content-length')

def comprueba(l):
	'''
	Si hay un fichero para descargar añade el tamaño
	'''
	if l[CAMPO].startswith('http'):
		l.append(sizeof(l[CAMPO]))


def add_size(ruta):
	'''
	Añade columna de tamaño a fichero csv
	'''
	f = open(ruta)
	lector = reader(f)
	lista = [l for l in lector]
	map(comprueba, lista)
	
	fw = open(ruta.replace('.csv', '_.csv'), 'w')
	escritor = writer(fw)
	escritor.writerows(lista)
	f.close()
	fw.close()

if __name__ == '__main__':
	for fichero in os.listdir(RUTA):
		print '-->', fichero 
		add_size(os.path.join(RUTA, fichero))






